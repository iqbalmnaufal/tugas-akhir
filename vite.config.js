import {sveltekit} from "@sveltejs/kit/vite";

/** @type {import('vite').UserConfig} */
const config = {
  plugins: [sveltekit()],
  test: {
    include: ["src/**/*.{test,spec}.{js,ts}"],
    environment: "jsdom",
    setupFiles: ["./src/setupTest.js"],
    coverage: {
      provider: "istanbul", // or 'c8'
      /*
        reference:
        * https://github.com/vitest-dev/vitest/issues/680
        * https://github.com/kouts/vitest-coverage/blob/main/vitest.config.ts
      */
      all: true,
      exclude: [
        "node_modules/**",
        ".svelte-kit",
        ".eslintrc.cjs",
        "playwright.config.js",
        "svelte.config.js",
        "tests",
        "src/data/types.d.ts",
        "src/app.d.ts",
        "vite.config.js"
      ],
    },
  },
};

export default config;
