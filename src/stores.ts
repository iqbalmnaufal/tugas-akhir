import {writable} from "svelte/store";

export const isTesting = writable(false);
export const page = writable({url: {pathname: "/"}});
export const theme = writable("Dark");
