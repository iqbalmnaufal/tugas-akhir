import {describe, it, expect, afterEach} from "vitest";
import {render, screen, fireEvent, cleanup} from "@testing-library/svelte";

import {theme} from "$src/stores/theme.js";
import Nav from "./Nav.svelte";

describe("Nav.svelte", () => {
  afterEach(() => {
    cleanup();
  });

  it("renders the navigation links", () => {
    render(Nav);
    expect(screen.getByText("Tentang")).toBeInTheDocument();
    expect(screen.getByText("Hiring?")).toBeInTheDocument();
    expect(screen.getByTestId("theme-switch")).toBeInTheDocument();
  });

  it("toggle button should change theme", async () => {
    render(Nav);
    const themeSwitchButton = screen.getByTestId("theme-switch");

    theme.set("Light");
    await fireEvent.click(themeSwitchButton);
    expect(localStorage.getItem("theme")).toEqual("Dark");

    // Test switching from Dark to Light theme
    theme.set("Dark");
    await fireEvent.click(themeSwitchButton);
    expect(localStorage.getItem("theme")).toEqual("Light");
  });

  it("testing", () => {
    render(Nav);
    expect(screen.getByText("Carikerja")).toBeInTheDocument();
  });

  it("theme switch button is rendered with the correct emoji and label", async () => {
    theme.set("Light");
    render(Nav);
    expect(screen.getByTestId("theme-switch")).toHaveTextContent("🌞 Light");
    cleanup();

    theme.set("Dark");
    render(Nav);
    expect(screen.getByTestId("theme-switch")).toHaveTextContent("🌜 Dark");
  });
});
