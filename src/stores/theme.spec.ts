import {describe, it, expect, afterEach} from "vitest";
import {get} from "svelte/store";
import {theme} from "./theme"; // Update with the correct path to your module

describe("Theme store tests", () => {
  it("should initialize with the correct theme", () => {
    const initialTheme = get(theme);
    expect(initialTheme).toBe("Light");
  });

  it("should toggle the theme and update the body class", async () => {
    // Set the theme to Dark
    theme.set("Dark");
    expect(get(theme)).toBe("Dark");
    expect(document.body.classList.contains("dark-mode")).toBeTruthy();

    // Set the theme back to Light
    theme.set("Light");
    expect(get(theme)).toBe("Light");
    expect(document.body.classList.contains("dark-mode")).toBeFalsy();
  });
});
