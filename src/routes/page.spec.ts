import {describe, it, expect, afterEach} from "vitest";
import {render, fireEvent, screen, cleanup} from "@testing-library/svelte";
import userEvent from "@testing-library/user-event";
import Page from "./+page.svelte";
import people from "$src/data/people";

describe("root component", () => {
  const people: Person[] = [
    {
      name: "Nurizko Maulana",
      status: "Fulltime, Remote",
      role: "Software Engineer, ",
      location: "Batam",
      social_media: {
        Linkedin: "https://www.linkedin.com/in/nurizko-maulana",
        Github: "https://github.com/nurizko-maulana",
        Blog: "https://www.nurizkomaulana.site/about",
      },
      tech_stack: ["NodeJS", "Flutter", "React", "Laravel", "PostgreSQL"],
      hired: false,
    },
    {
      name: "Doddy Rizal Novianto",
      status: "Fulltime, Remote",
      role: "Frontend Developer",
      location: "Jakarta, Tangerang",
      social_media: {
        Linkedin: "https://www.linkedin.com/in/doddy-rizal-novianto-559269157/",
        Github: "https://github.com/drzaln",
      },
      tech_stack: ["react-native", "react", "javascript"],
      hired: false,
    },
  ];

  afterEach(() => {
    cleanup();
  });

  it("should sort people by name", async () => {
    render(Page, {sortedPeople: people});
    people.forEach((p) => {
      const name = screen.getByText(p.name);
      const status = screen.getAllByText(`⏲️ ${p.status}`);
      // const role = screen.getAllByText(`💻 ${p.role}`)
      const location = screen.getByText(`📍 ${p.location}`);

      expect(name).toBeInTheDocument();
      expect(status[0]).toBeInTheDocument();
      // expect(role).toBeInTheDocument()
      expect(location).toBeInTheDocument();
    });
  });

  it('should display "HIRED!" when person is hired', () => {
    // Set the 'hired' property of the first person to true
    const hiredPerson = {...people[0], hired: true};

    render(Page, {props: {sortedPeople: [hiredPerson]}});

    const hiredText = screen.getByText("HIRED!");
    expect(hiredText).toBeInTheDocument();
  });

  it('should not display "HIRED!" when person is not hired', () => {
    // Set the 'hired' property of the first person to false
    const notHiredPerson = {...people[0], hired: false};

    render(Page, {props: {sortedPeople: [notHiredPerson]}});

    const hiredText = screen.queryByText("HIRED!");
    expect(hiredText).not.toBeInTheDocument();
  });

  it("should display Linkedin link when social_media is a string", () => {
    const personWithSocialMediaString = {
      ...people[0],
      social_media: "https://www.linkedin.com/in/sample",
    };

    render(Page, {props: {sortedPeople: [personWithSocialMediaString]}});

    const linkedinLink = screen.getByText("Linkedin");
    expect(linkedinLink).toBeInTheDocument();
    expect(linkedinLink.getAttribute("href")).toBe(
      "https://www.linkedin.com/in/sample",
    );
  });

  it("should display social media links when social_media is an object", () => {
    const personWithSocialMediaObject = {
      ...people[0],
      social_media: {
        Linkedin: "https://www.linkedin.com/in/sample",
        Twitter: "https://twitter.com/sample",
      },
    };

    render(Page, {props: {sortedPeople: [personWithSocialMediaObject]}});

    const linkedinLink = screen.getByText("Linkedin");
    expect(linkedinLink).toBeInTheDocument();
    expect(linkedinLink.getAttribute("href")).toBe(
      "https://www.linkedin.com/in/sample",
    );

    const twitterLink = screen.getByText("Twitter");
    expect(twitterLink).toBeInTheDocument();
    expect(twitterLink.getAttribute("href")).toBe("https://twitter.com/sample");
  });

  it("should filter people by location", async () => {
    const {container} = render(Page, {sortedPeople: people});

    const locationSelect = container.querySelector("#inputLocation");
    if (locationSelect) {
      // Type the value 'Bandung' in the input field
      await userEvent.type(locationSelect, "Bandung");

      // Press 'Enter' key to select the value
      await fireEvent.keyDown(locationSelect, {key: "Enter", code: "Enter"});

      const filteredLocations = await screen.findAllByText(/📍/i);
      expect(filteredLocations[0]).toHaveTextContent("📍 Bandung");
    } else {
      throw new Error("locationSelect is not found");
    }
  });

  it("should filter people by tech stack", async () => {
    const {container} = render(Page, {sortedPeople: people});

    const locationSelect = container.querySelector("#inputTechStack");
    if (locationSelect) {
      // Type the value 'Bandung' in the input field
      await userEvent.type(locationSelect, "Preact");

      // Press 'Enter' key to select the value
      await fireEvent.keyDown(locationSelect, {key: "Enter", code: "Enter"});

      const filteredLocations = await screen.findAllByText(/⚙️/i);
      expect(filteredLocations[0]).toHaveTextContent("Preact");
    } else {
      throw new Error("locationSelect is not found");
    }
  });
});
