import {describe, it, expect, afterEach} from "vitest";
import {render, screen, cleanup} from "@testing-library/svelte";

import Page from "./+page.svelte";

const assertDefined = <T>(obj: T | null | undefined): T => {
  expect(obj).toBeDefined();
  return obj as T;
};

describe("should render about component correctly", () => {
  afterEach(() => {
    cleanup();
  });

  it("should render heading (h1)", () => {
    render(Page);
    const header = screen.getByText("Tentang Carikerja");
    expect(header).toBeInTheDocument();
  });

  it("should render paragraph", () => {
    render(Page);
    const paragraph = screen.getByText(
      "Kami menghubungkan para Software Engineer kece di tanah air yang terpaksa harus terkena pemutusan hubungan kerja karena pandemi COVID-19 dengan perusahaan yang sedang mencari talenta digital.",
    );
    expect(paragraph).toBeInTheDocument();
  });

  it("should render support heading (h4)", () => {
    render(Page);
    const header = screen.getByText("Ingin menambahkan seseorang?");
    expect(header).toBeInTheDocument();
  });

  it("render Github link", () => {
    const {container} = render(Page);
    const githubLinkRepo = assertDefined(
      container.querySelector(
        'a[href="https://github.com/rizafahmi/carikerja"]',
      ),
    );
    expect(githubLinkRepo.textContent).toContain("Clone repositori ini");
    const githubLinkData = assertDefined(
      container.querySelector(
        'a[href="https://github.com/rizafahmi/carikerja/edit/main/src/data/employer.js"]',
      ),
    );
    expect(githubLinkData.textContent).toContain("Github");
  });

  it("render code tag for repo link", () => {
    render(Page);
    const codeTag = screen.getByText("src/data/employer.js");
    expect(codeTag.tagName).toBe("CODE");
  });
});
