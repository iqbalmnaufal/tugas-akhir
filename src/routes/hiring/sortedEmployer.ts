import employers from "$src/data/employer.js";

const sortEmployers = (inputEmployers: Employer[]) => {
  return inputEmployers.sort((a, b) => {
    return a.name.localeCompare(b.name, undefined, {sensitivity: "base"});
  });
};

const sortedEmployer = sortEmployers(employers);

export {sortedEmployer, sortEmployers};
