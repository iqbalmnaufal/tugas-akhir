import { describe, it, expect, beforeEach, afterEach } from "vitest";
import { render, screen, cleanup } from "@testing-library/svelte";

import Page__SvelteComponent_ from "./+page.svelte";
import { sortEmployers, sortedEmployer } from "./sortedEmployer";

const assertDefined = <T>(obj: T | null | undefined): T => {
  expect(obj).toBeDefined();
  return obj as T;
};

describe("hiring page", () => {
  // const { container } = render(Page__SvelteComponent_, { employers: sortedEmployer });
  // beforeEach(() => {
  // });

  afterEach(() => {
    cleanup();
  });

  it("should correctly sort employers by name", () => {
    const employers: Employer[] = [
      {
        name: "Company C",
        category: "Tech",
        link: "https://company-c.com",
        description: "Company C description",
      },
      {
        name: "Company A",
        category: "Finance",
        link: "https://company-a.com",
        description: "Company A description",
      },
      {
        name: "Company B",
        category: "Tech",
        link: "https://company-b.com",
        description: "Company B description",
      },
    ];

    const sortedEmployers = sortEmployers(employers);

    expect(sortedEmployer).toBeDefined();

    expect(sortedEmployers).toEqual([
      {
        name: "Company A",
        category: "Finance",
        link: "https://company-a.com",
        description: "Company A description",
      },
      {
        name: "Company B",
        category: "Tech",
        link: "https://company-b.com",
        description: "Company B description",
      },
      {
        name: "Company C",
        category: "Tech",
        link: "https://company-c.com",
        description: "Company C description",
      },
    ]);
  });

  it("should display the correct title text", async () => {
    render(Page__SvelteComponent_);
    const title = document.querySelector("h1");
    if (title) {
      if(title !== null){

        const expectedString = "Untukmu yang terdampak, temukan beberapa perusahaan yang masih membuka lowongan:";
        const actualString = (title.textContent ?? "").replace(/\s+/g, ' ').trim();
        expect(actualString).toBe(expectedString);
      }
      // expect(title.textContent).toBe(
      //   "Untukmu yang terdampak, temukan beberapa perusahaan yang masih membuka lowongan",
      // );
    } else {
      throw new Error("Title element not found");
    }
  });

  it("should render list of employers", () => {
    render(Page__SvelteComponent_, { sortedEmployer: sortedEmployer as never });

    if (sortedEmployer.length > 0) {
      sortedEmployer.forEach((employer) => {
        // Check if the employer name is rendered correctly
        const nameElement = screen.getByText(employer.name);
        expect(nameElement).toBeInTheDocument();

        // Check if the employer description is rendered correctly
        const descriptionElement = screen.getByText(employer.description);
        expect(descriptionElement).toBeInTheDocument();
      });
    } else {
      // Check if the "data perusahaan belum tersedia" message is rendered correctly
      const messageElement = screen.getByText("data perusahaan belum tersedia");
      expect(messageElement).toBeInTheDocument();
    }
  });

  // it('should render a list of employers if employers is exist', () => {
  //   const employers: Employer[] = [
  //     { name: 'Company C', category: 'Tech', link: 'https://company-c.com', description: 'Company C description' },
  //     { name: 'Company A', category: 'Finance', link: 'https://company-a.com', description: 'Company A description' },
  //     { name: 'Company B', category: 'Tech', link: 'https://company-b.com', description: 'Company B description' },
  //   ];

  //   const { container } = render(Page__SvelteComponent_, { sortedEmployer: employers });

  //   employers.forEach((employer) => {
  //     // Check if the employer name is rendered correctly
  //     const anchorSelector = `a[href="${employer.link}"]:contains("${employer.name}")`;
  //     const nameElement = container.querySelector(anchorSelector);
  //     expect(nameElement).toBeTruthy();

  //     // Check if the employer description is rendered correctly
  //     const descriptionSelector = `p:contains("${employer.description}")`;
  //     const descriptionElement = container.querySelector(descriptionSelector);
  //     expect(descriptionElement).toBeTruthy();
  //   });
  // })

  // it('renders "data perusahaan belum tersedia" if no employers exist', () => {
  //   const { container } = render(Page__SvelteComponent_, { sortedEmployer: [] });
  //   console.log(screen.debug())
  //   // console.log(container)

  //   const messageElement = container.querySelector('li:contains("data perusahaan belum tersedia")');
  //   expect(messageElement).toBeTruthy();
  // });

  // it('should render a list of employers', () => {
  //   if (sortedEmployer && Array.isArray(sortedEmployer) && sortedEmployer.length > 0) {
  //     sortedEmployer.forEach((employer: Employer) => {
  //       expect(screen.getByText(employer.name)).toBeInTheDocument();
  //       expect(screen.getByText(employer.description)).toBeInTheDocument();
  //     });
  //   } else {
  //     expect(screen.getByText('data perusahaan belum tersedia')).toBeInTheDocument();
  //   }
  // })

  it("should render how to add data instruction correctly", () => {
    const { container } = render(Page__SvelteComponent_, {
      employers: sortedEmployer,
    });
    expect(
      screen.getByText("Ingin perusahaanmu ada di daftar kami?"),
    ).toBeInTheDocument();

    const githubLink = assertDefined(
      container.querySelector(
        'a[href="https://github.com/rizafahmi/carikerja"]',
      ),
    );
    expect(githubLink.textContent).toContain("Clone repositori ini");

    // expect(screen.getByText('dan tambahkan data baru di')).toBeInTheDocument();
    expect(
      screen.getByText("src/data/employer.js", { selector: "code" }),
    ).toBeInTheDocument();
    // expect(screen.getByText('. Bisa juga edit langsung via')).toBeInTheDocument();
  });
});
