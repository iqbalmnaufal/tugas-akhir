import {describe, it, expect, test} from "vitest";
import {render, screen} from "@testing-library/svelte";
import Layout from "./+layout.svelte";

const assertDefined = <T>(obj: T | null | undefined): T => {
  expect(obj).toBeDefined();
  return obj as T;
};

describe("MainLayout.svelte", () => {
  it("renders the Nav component", async () => {
    render(Layout);

    const navElement = await screen.findByRole("navigation");

    expect(navElement).toBeInTheDocument();
  });

  it("renders DeepTech Foundation link", async () => {
    const {container} = render(Layout);
    const deepTechLink = assertDefined(
      container.querySelector('a[href="https://deeptech.id"]'),
    );
    expect(deepTechLink.textContent).toContain("DeepTech Foundation");
  });

  it("renders GitHub link", () => {
    const {container} = render(Layout);
    const gitHubLink = assertDefined(
      container.querySelector(
        'a[href="https://github.com/rizafahmi/carikerja"]',
      ),
    );
    expect(gitHubLink.textContent).toContain("GitHub");
  });
});
